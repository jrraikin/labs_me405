import pyb
##Set LED 2 high then low
'''
pinA5 = pyb.Pin.cpu.A5
MyMapperDict = {'LED2' : pinA5}
pyb.Pin.dict(MyMapperDict)
pinA5 = pyb.Pin("LED2", pyb.Pin.OUT_PP)
pinA5.high()
#pinA5.low()
'''

#Adjust LED brightness
pinA5 = pyb.Pin.cpu.A5
tim2 = pyb.Timer(2, freq = 2000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
t2ch1.pulse_width_percent(10)
#t2ch1.pulse_width_percent(50)
#t2ch1.pulse_width_percent(100)
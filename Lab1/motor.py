''' @file motor.py
Lab 1: (Description)'''
#Josh Raikin
import pyb

def main():
    motor_driver(2000, 10, 'cw')

## input frequency, pwm, and direction('ccw' or 'cw')
def motor_driver(Freq, pwm, direc):
    pinA10 = pyb.Pin.cpu.A10
    pinB4 = pyb.Pin.cpu.B4
    pinB5 = pyb.Pin.cpu.B5
    
    #turn ENA
    ENA = pyb.Pin(pinA10, pyb.Pin.OUT_PP)
    ENA.high()
    
    # 'ccw' or 'cw'
    if (direc == 'ccw'):
        #set IN1A low
        IN1A = pyb.Pin(pinB4, pyb.Pin.OUT_PP)
        IN1A.low()
        #set IN2A to PWM
        tim3 = pyb.Timer(3, freq= Freq)
        t3ch2 = tim3.channel(2, pyb.Timer.PWM, pin=pinB5)
        t3ch2.pulse_width_percent(pwm)
    elif (direc == 'cw'):
        #set IN2A low
        IN2A = pyb.Pin(pinB5, pyb.Pin.OUT_PP)
        IN2A.low()
        #set IN2A to PWM
        tim3 = pyb.Timer(3, freq= Freq)
        t3ch1 = tim3.channel(1, pyb.Timer.PWM, pin=pinB4)
        t3ch1.pulse_width_percent(pwm)
        
        
    

if __name__ == '__main__':
    main()
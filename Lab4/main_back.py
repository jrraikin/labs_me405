''' @file main.py
Josh Raikin
Lab 4: This lab utilizes a proportional controller in order to operate the motor, specifically using a step response.
Link to code: https://bitbucket.org/jrraikin/labs_me405/src/master/Lab4/main.py
'''
from pyb import I2C
import utime


def main():
    reg_mode = const(0x3D)
    reg_calib = const(0x35)

    reg_PITCH_LSB = const(0x1E)
    reg_ROLL_LSB = const(0x1C)
    reg_YAW_LSB = const(0x1A)

    reg_avel_x_LSB = const(0x14)
    reg_avel_y_LSB = const(0x16)
    reg_avel_z_LSB = const(0x18)

    reg_grav_x_LSB = const(0x2E)
    reg_grav_y_LSB = const(0x30)
    reg_grav_z_LSB = const(0x32)

def main():
    imu = IMU(1)
    imu.calibration_stat()
    imu.euler_angles()

    i = 0
    while(i < 200):

        print("\nGravity: ")
        print("\t", imu.gravity())

        print("\nEuler Angles: ")
        print("\t", imu.euler_angles())

        print("\nAngle Velocities: ")
        print("\t", imu.ang_veloc())

        utime.sleep_ms(1000)
        i += 1
    imu.disable_IMU()
    #print(byte_to_bit(b'\x02\x01'))


class IMU:

    def __init__(self, bus):
        self.bus = bus
        self.i2c = I2C(self.bus, I2C.MASTER)
        self.baud = 400000
        add_respond =  self.i2c.scan()
        self.address = 0
        #check to see add is 40 (IMU)
        for add in add_respond:
            if add == 40:
                self.address = add
                break
        #if add == 40, enable IMU
        if(self.address is 40):
            if(self.i2c.is_ready(self.address)):
                self.mode_IMU(0b1100) #Fusion Mode, NDOF
                self.enable_IMU()
        else:
            self.disable_IMU()


    def enable_IMU(self):
        print('enabling IMU...')
        self.calibration_stat()
        i = 0
        while (self.calib_stat != (3,3,3,3) and i < 2000):
            print('Calibration:', self.calib_stat, '\ti:', i)
            self.calibration_stat()
            i += 1
        print('IMU Calibrated')

    def disable_IMU(self):
        print('\ndisabling IMU...')
        self.i2c.deinit()

    def mode_IMU(self, mode):
        self.buff = bytearray(1)
        self.buff[0] = mode
        self.i2c.mem_write(self.buff, 40, 0x3D,timeout=5000, addr_size = 8)

    def calibration_stat(self):
        #Calibration Status
        calibs = self.i2c.mem_read(1, self.address, reg_calib, timeout = 5000, addr_size = 8)
        self.SYS_calib = (calibs[0] & 0b11000000) >> 6
        self.GYR_calib = (calibs[0] & 0b00110000) >> 4
        self.ACC_calib = (calibs[0] & 0b00001100) >> 2
        self.MAG_calib = (calibs[0] & 0b00000011) >> 0
        self.calib_stat = (self.SYS_calib, self.GYR_calib, self.ACC_calib, self.MAG_calib)
        return self.calib_stat

    def euler_angles(self):
        #PITCH
        euler_ang_pitch = (self.i2c.mem_read(2, self.address, reg_PITCH_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(euler_ang_pitch)
        self.euler_ang_pitch = val / 16

        #ROLL
        euler_ang_roll = (self.i2c.mem_read(2, self.address, reg_ROLL_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(euler_ang_roll)
        self.euler_ang_roll = val / 16

        #HEADING
        euler_ang_head = (self.i2c.mem_read(2, self.address, reg_YAW_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(euler_ang_head)
        self.euler_ang_head = val / 16

        self.angles = (self.euler_ang_head, self.euler_ang_roll, self.euler_ang_pitch)
        return(self.angles)

    def ang_veloc(self):
        #X
        ang_v_x = (self.i2c.mem_read(2, self.address, reg_avel_x_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(ang_v_x)
        self.ang_v_x = val

        #Y
        ang_v_y = (self.i2c.mem_read(2, self.address, reg_avel_y_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(ang_v_y)
        self.ang_v_y = val

        #Z
        ang_v_z = (self.i2c.mem_read(2, self.address, reg_avel_z_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(ang_v_z)
        self.ang_v_z = val

        self.velocs = (self.ang_v_x, self.ang_v_y, self.ang_v_z)
        return(self.velocs)


    def gravity(self):
        #X
        g_x = (self.i2c.mem_read(2, self.address, reg_grav_x_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(g_x)
        self.g_x = val

        #Y
        g_y = (self.i2c.mem_read(2, self.address, reg_grav_y_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(g_y)
        self.g_y = val

        #Z
        g_z = (self.i2c.mem_read(2, self.address, reg_grav_z_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(g_z)
        self.g_z = val

        self.grav = (self.g_x, self.g_y, self.g_z)
        return(self.grav)


def byte_to_bit(byte):
    val = (byte[0] | byte[1]<<8)
    if (val > 32767):
        val -= 65536
    elif (val < -32767):
        val += 65536
    return val


if __name__ == '__main__':
    main()

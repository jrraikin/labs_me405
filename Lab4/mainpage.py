## @file mainpage.py
#  @author Josh Raikin
#
#  Detailed doc for mainpage.py
#
#  @mainpage
#
#  @section sec_intro Introduction
#  Lab 4: This Lab allows for using an IMU to collect euler angles and angular velocity, \n
#    \t with the addition of gravity and temperature as a bonus.
#    \t Link to code: https://bitbucket.org/jrraikin/labs_me405/src/master/Lab4/main.py
#
#
#  @section sec_out Output
#  While loop produces print results of updated gravity, euler angles, angular velocity, and temperature.
#
#
#  @section sec_vid Video of Operation
#  Link to video of operation: \n
#  https://drive.google.com/file/d/1NJBbGoZUUttRatJgePb0_L4wphIrSLUr/view?usp=sharing
#
#
#  @section sec_imu IMU
#  Sets up IMU parameters \n
#  Initiliazes IMU by: \n
#  - establishing I2C with MASTER connection \n
#  - checking to see that the right address is scanned for "40" \n
#  - if the right address is scanned for, enabling IMU with the correct mode (NDOF) \n
#  - else: disable IMU \n
#  @param bus    establishes bus for the connection
#
#
#  @section sec_enable Enable IMU
#  Enables the IMU. Checks that every calibration value is 3 before passing. \n
#  Calibration values: SYS, GYR, ACC, MAG
#
#
#  @section sec_disable Disable IMU
#  Disables IMU.
#
#
#  @section sec_mode Mode
#  Sets mode of IMU. NDOF is the only mode accepted to get values desires. \n
#  @param mode   mode of IMU can be inputted. If not, False is returned.
#  @return       True if mode matches NDOF, False otherwise
#
#
#  @section sec_calib Calibration Status
#  Checks calibration status for SYS, GYR, ACC, MAG and shifts bit values as necessary.
#  @return Tuple of all values (0-3) returned.
#
#
#  @section sec_euler_ang Euler Angles
#  Finds euler angles for pitch, roll, and heading/yaw. Divides aquired value by 16 to get correct result.
#  @return Tuple of all values (degrees).
#
#
#  @section sec_ang_veloc Angular Velocity
#  Find angular velocities (x, y, z) using gyroscope.
#  @return Tuple of (x,y,z) returned.
#
#
#  @section sec_grav Gravity
#  Find gravity in x,y, and z direction in m/s^2.
#  @return Tuple of (x, y, z) returned.
#
#
#  @section sec_temp Temperature
#  Temperature determined in degrees.
#  Defualt fahrenheit, unless 'C' is inputted.
#  @return Temperature output in desired unit.
#
#
#  @section sec_bit_byte Register Data
#  Takes in byte data from register and shifts it to collect it accurately.
#  @param byte Data collectedc from registers LSB and MSB.
#  @return value of registers data returned.
#
#
#
#
#
#  @copyright License Info
#
#  @date May 13, 2020
#

close all;

Kp055.Properties.VariableNames{1} = 'time';
Kp055.Properties.VariableNames{2} = 'pos';

Kp05.Properties.VariableNames{1} = 'time';
Kp05.Properties.VariableNames{2} = 'pos';

Kp04.Properties.VariableNames{1} = 'time';
Kp04.Properties.VariableNames{2} = 'pos';

Kp03.Properties.VariableNames{1} = 'time';
Kp03.Properties.VariableNames{2} = 'pos';


figure;
plot(Kp03.time, Kp03.pos, Kp04.time, Kp04.pos, Kp05.time, Kp05.pos, Kp055.time, Kp055.pos)
title('Setpoint 20000, Kp=x')
ylabel('Position');
xlabel('time (ms)');
legend('Kp=0.03', 'Kp=0.04', 'Kp=0.05', 'Kp=0.055', 'Location', 'Southeast');

% figure;
% plot(Kp03.time, Kp03.pos)
% title('Setpoint 20000, Kp=0.03')
% ylabel('Position');
% xlabel('time (ms)');
% 
% figure;
% plot(Kp04.time, Kp04.pos)
% title('Setpoint 20000, Kp=0.04')
% ylabel('Position');
% xlabel('time (ms)');
% 
% figure;
% plot(Kp05.time, Kp05.pos)
% title('Setpoint 20000, Kp=0.05')
% ylabel('Position');
% xlabel('time (ms)');
% 
% figure;
% plot(Kp055.time, Kp055.pos)
% title('Setpoint 20000, Kp=0.055')
% ylabel('Position');
% xlabel('time (ms)');

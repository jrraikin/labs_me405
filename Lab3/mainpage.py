## @file mainpage.py
#  @author Josh Raikin
#
#  Detailed doc for mainpage.py
#
#  @mainpage
#
#  @section sec_intro Introduction
#  Lab 3: This lab utilizes a proportional controller in order to operate the motor, specifically using a step response. \n
#         Link to code: https://bitbucket.org/jrraikin/labs_me405/src/master/Lab3/main.py
#  @section sec_out Output
#  @image html step_plot.jpg "All of the plots in one" width=50%
#  Below are the individual step plots:
#  @image html K3.jpg "Kp = 0.03" width=40%
#  @image html K4.jpg "Kp = 0.04" width=40%
#  @image html K5.jpg "Kp = 0.05" width=40%
#  @image html K55.jpg "Kp = 0.055" width=40%
#
#  @section sec_tune Tuning
#  As seen below in a zoomed in image of every plot, Kp=0.055 showed the best performance \n
#  as it had no overhsoot and arrived at the desired value first (20,000).
#  @image html zoom_all.jpg "Zoomed in of all plots" width=50%
#  Tuning required changing the Kp value until the least overshoot and best time performance were visualized. \n
#  In addition to most accurate.
#
#  @section sec_vid Video of Operation
#  Link to video of operation: \n
#  https://drive.google.com/file/d/17hJRsoHEunFhOWFsv0Qve2rqoV0sm-Y0/view?usp=sharing
#
#  @section sec_test Testing
#  Upon launching, main will prompt for a setpoint value (integer only) and a Kp value (float or integer only). \n
#  Kp is best when used between the values of 0 and 1. \n
#  Motor 1 is initialized with selected pins:
#  \code{.py}
#  #Encoder
#  timerE = 4 #page 93 datasheet
#  pin_encA = pyb.Pin.cpu.B6
#  pin_encB = pyb.Pin.cpu.B7
#  #Motor
#  timerM = 3
#  pin_EN = pyb.Pin.cpu.A10
#  pin_IN1 = pyb.Pin.cpu.B4
#  pin_IN2 = pyb.Pin.cpu.B5
#  \endcode
#  However, motor 2 is an option too:
#  \code{.py}
#  #Encoder
#  timerE = 8
#  pin_encA = pyb.Pin.cpu.C6
#  pin_encB = pyb.Pin.cpu.C7
#  #Motor (for use of IN1B & IN2B)
#  timerM = 5
#  pin_EN = pyb.Pin.cpu.C1
#  pin_IN1 = pyb.Pin.cpu.A0
#  pin_IN2 = pyb.Pin.cpu.A1
#  \endcode
#  The encoder, motor drive, and controller are all initialized. \n
#  Then, for 200 cycles (within a while loop) the controller updates and collects the time and position. \n
#  The motor is then disabled.
#
#
#  @section sec_cont Controller
#  Sets up the controller parameters \n
#  @param setpoint    The setpoint taken from main is used to know what value to go to for the step response
#  @param Kp          This value (should be within range of 0 and 1 for bets results), sets the proportional scale for the response
#  @param encoder_class    Location class object taken from Lab 2 (Encoder). Used to find position.
#  @param motor_class      Motor class object taken from Lab 1. Used to drive motor.
#
#  @section sec_upd Update
#  When called regularly, updates the error value by finding the new position and subtracting it from the setpoint. \n
#  Uses the error value obtained to determine the Actuation signal, which gets inputted into the motor driver.
#
#
#  @section sec_print Print Tests
#  Formats gathered inputs from while loop in order to make a csv file for the graphs to be made. \n
#  If somehow the lists do not have the same length, the length of the shorter list is used to not have an index error.
#  @param time    list of time (200 samples)
#  @param mot_pos motor position list (200 samples)
#
#
#
#  @copyright License Info
#
#  @date May 13, 2020
#

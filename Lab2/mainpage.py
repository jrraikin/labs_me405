## @file mainpage.py
#  @author Josh Raikin
#
#  Detailed doc for mainpage.py
#
#  @mainpage
#
#  @section sec_intro Introduction
#  Lab 2: This Lab allows one to get updates on how much the motor has spun, in either direction, and accounts for overflow. \n
#         Link to code: https://bitbucket.org/jrraikin/labs_me405/src/master/Lab2/main.py
#
#
#  @section sec_test Testing
#  set timer: timer = 4 (for motor 1, 8 for motor 2) \n
#  set encoder A pin: pin_encA = pyb.Pin.cpu.B6 (for motor 1, pyb.Pin.cpu.C6 for motor 2) \n
#  set encoder B pin: pin_encB = pyb.Pin.cpu.B7 (for motor 1, pyb.Pin.cpu.C7 for motor 2) \n
#  To run class and receive updates continuously for position and delta:
#  \code{.py}
#  enc = Encoder(pin_encA, pin_encB, timer)
#  while(1):
#      enc.update()
#      print("position: {0}".format(enc.get_position()))
#      print("delta: {0}".format(enc.get_delta()))
#      utime.sleep_ms(500) #delay so position update is slower
#  \endcode
#
#  @section sec_enc Encoder Driver
#  Does the setup, given appropriate parameters such as which pins and timer to use. \n
#  Also configures channels and establishes the first lastRead value.
#  @param enc_A    A pyb.Pin that establishes encoder A's pin
#  @param enc_B    A pyb.Pin that establishes encoder B's pin
#  @param timer    Input for appropriate timer, as motor A pins and motor B pins use different timers#
#
#  @section sec_upd Update
#  When called regularly, updates the recorded position of the encoder. \n
#  Find the delta value, and adds it to running total.
#      If delta value happens to go over or below the period / 2,
#      (2^16 -1) is deducted or added from/to it.
#
#  @section sec_get_pos Get Position
#  Returns the most recently updated position of the encoder (self.runningTot).
#
#  @section sec_set_pos Set Position
#  Resets the position to to a specified value.
#  @param value    Input for desired location of motor.
#
#  @section sec_get_del Get Delta
#  Returns the difference in recorded position between the two most recent calls to update().
#
#
#  @copyright License Info
#
#  @date May 6, 2020
#

''' @file ME405_LAB0.py
This file prompts the user for number index of the Fibbonacci sequence, 
and returns the solution for that index.
Link to code: https://bitbucket.org/jrraikin/labs_me405/src/master/Lab0/ME405_LAB0.py'''
#Josh Raikin

def main():
    while(True): # fib() will return 0 if user wants to exit program
        if (fib() == 0):
            break
            
## function to run fibbonacci code
def fib():
        try: #try condition for integer, if failed will prompt for correct input or exit program
            idx = input("Input index desired for Fibbonacci: ") # user input for code
            idx = int(idx)
            print ('Calculating Fibonacci number at index n = {:}.'.format(idx))
            sums = 0
            old = 0
            if (idx == 0):
                sums = 0;
                print('Solution: {:d}'.format(sums))
            elif (idx != '\n' or idx != 'q'): # elif condition that if \n or q is entered, this section is skipped
                old = 0
                sums =1
                hold = 0
                for i in range(0, idx - 1): #handles float or double case to exit "try"
                    hold = sums
                    sums += old
                    old = hold
                print('Solution: {:d}'.format(sums))
            return 1
        except: # exit condition, assumes user won't enter anything except 'y' or 'n'
            print("\n*Input only accepts integers.") # tells user what input allows
            fin = input("\tExit program? (y/n): ")
            if (fin == 'y'):
                print("exiting...\n")
                return 0
            else:
                fib()

if __name__ == '__main__':
    main()
